\contentsline {section}{\numberline {1}Aufgabenstellung und Zielsetzung}{2}
\contentsline {section}{\numberline {2}Versuchsaufbau}{3}
\contentsline {section}{\numberline {3}Modellierung}{4}
\contentsline {subsection}{\numberline {3.1}Positionsbestimmung der Kugel}{4}
\contentsline {subsection}{\numberline {3.2}Servomotor Ansteuerung}{4}
\contentsline {subsection}{\numberline {3.3}Regelung der Wippe}{5}
\contentsline {section}{\numberline {4}Implementierung}{6}
\contentsline {subsection}{\numberline {4.1}Pin-Belegung der Komponenten}{6}
\contentsline {subsection}{\numberline {4.2}Die Initialisierung}{6}
\contentsline {subsection}{\numberline {4.3}Implementierung der Pulsweitenmodulation}{7}
\contentsline {subsection}{\numberline {4.4}Die Regelungsfunktion}{7}
\contentsline {subsubsection}{\numberline {4.4.1}Serielle Ausgabe der Position}{8}
\contentsline {section}{\numberline {5}Evaluierung}{9}
\contentsline {subsection}{\numberline {5.1}Die Pulsweitenmodulation}{9}
\contentsline {subsection}{\numberline {5.2}Die Regelung der Wippe}{10}
\contentsline {subsection}{\numberline {5.3}Ausgabe der Strecke zur Mitte}{11}
\contentsline {subsection}{\numberline {5.4}Die Regelstrecke der Regelungsfunktion}{11}
\contentsline {section}{\numberline {A}Quellcode}{12}
