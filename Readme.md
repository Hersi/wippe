# Projekt Wippe

### 1. Aufgabenstellung und Zielsetzung

Ziel dieser Arbeit ist es, eine Metallkugel auf einer Wippe mit einem Arduino UNO in der Mitte zu balancieren. Zus�tzlich soll die Position der Kugel in "'mm"' zur Mitte als Serielle Ausgabe �ber die IDE-Schnittstelle ausgegeben werden.

### 2. Versuchsaufbau

F�r diesen Versuch wurde folgende Hardware zur Verf�gung gestellt:
- 1x Arduino UNO
- 1x Servo-Motor
- 1x fest verbaute Wippe
- 1x Metallkugel
- 2x Folienpotentiometer

Der verwendete Arduino ist von Chipkit und besitzt einen PIC32-Mikrocontroller. 

Der Servo-Motor ist f�r die Neigung der Wippe verantwortlich und hat einen maximalen Drehwinkel von 180�.

Die Wippe ist auf einem Holzbrett fest verbaut.

Die Metallkugel wird f�r das balancieren auf der Wippe genutzt. Diese ist leider keine perfekte "'Kugel"' und ist ein wenig deformiert.

Die Folienpotentiometer werden f�r die Bestimmung der Position der Metallkugel verwendet. Die Formel f�r die Spannung am Ausgang f�r den Potentiometer wurde wie folgt vorgegeben:
U_{A/D} = 3,3V * \frac{1-s}{1+s} mit  0 < s < 1
### 3. Modellierung
#### Positionsbestimmung

F�r die Positionsbestimmung der Metallkugel werden, wie im Kapitel zuvor erw�hnt, Folienpotentiometer als Sensoren verwendet. Diese sind entsprechend an die Wippe angeklebt damit die richtige Position ermittelt werden kann. Je nach Position werden Analogwerte an den Arduino �bertragen. Die Sensoren geben Werte zwischen 0 und 1023 aus. Leider verl�uft die Formel nicht Linear, weswegen man eine Umformung der Formel vornehmen muss um die Strecke zu erhalten. Dabei wird diese nach s aufgel�st:

U_{A/D} = 3,3V * \frac{1-s}{1+s}

in 

s = \frac{1.0-U_{A/D}/3,3V}{1.0+U_{A/D}/3,3V}

Die 3,3V entsprechen dem Wert 1023 f�r die L�nge des Potentiometers.

#### Ansteuerung des Servomotors

Gesteuert wird der Servo durch eine Pulsweitenmodulation(PWM) zwischen einer Millisekunde und zwei Millisekunden. Bei einer PWM von 1,5ms soll der Servo in der Mitte des Geltungsbereiches liegen. Dies ist bei dem verwendeten Servomotor ein 90� Winkel. Bei dieser Position sollte die Wippe gerade(also keine Neigung) ausgerichtet sein.  

Die Aufgabe besteht darin eine PWM ohne zus�tzliche Arduino Servo Bibliotheken zu realisieren. Dabei gibt es verschiedene M�glichkeiten. Zum einen kann mit Zeiten (im Bereich der Mikrosekunden) gearbeitet werden. Zum anderen k�nnen die von Chipkits Arduino Uno unterst�tzten "'Core Timer"' verwendet werden. Oder direkt in den Register des Mikrocontrollers (Output Compare) schreiben.

F�r den Einstieg zum Programmieren des Arduino's wurde auf die Implementierung mittels Mikrosekunden entschieden.

#### Regelung der Wippe

F�r das balancieren der Kugel an einer bestimmten Position wird ein Regler ben�tigt. Dieser Regler berechnet die Differenz der Soll- und Ist-Position. Soll-Position in dieser Aufgabe ist die Mitte der Strecke und die Ist-Position die aktuelle Position der Kugel auf der Wippe. Es gibt verschiedene Regelungsverfahren um dies zu realisieren. Zum einen gibt es den relativ einfachen "'Proportionalregler"'. Dieser gibt einen konstanten Verst�rkungsfaktor der Differenz an. Ein weiterer Regler w�re ein PD-Regler ("'Proportional-Differential"'). Zus�tzlich zum P-Regler wird ein D-Anteil hinzugef�gt. Dieser teil weist ein differenzierendes �bertragungsverhalten auf. Die Amplitude der Ausgangsgr��e ist von der �nderungsgeschwindigkeit der Eingangsgr��e abh�ngig. Um die Regelung noch schneller auszuf�hren, kann ein I-Anteil hinzugef�gt werden. Dieser Teil weist ein integratives �bertragungsverhalten auf. Mit diesem Teil kann die �nderungsgeschwindigkeit von der H�he der Amplituden�nderung der Eingangsgr��e bestimmt werden.
Alle Regelungsanteile kombiniert ergibt den "'PID-Regler"'. Zusammengefasst regelt dieser Regler deutlich schneller die Kugel in die Mitte als ein einfacher P-Regler. 

Durch "'probieren"' werden die ben�tigten Verst�rkungsfaktoren der einzelnen Regelungsanteile ermittelt.

In dieser Aufgabe wird die Mitte als Zielposition angegeben. F�r die Regelung der Wippe wird ein PID-Regler verwendet.

### 4. Implementierung
#### Pinbelegung der Komponenten
F�r die Stromzufuhr und die Ansteuerung des Servomotors sowie zum Auslesen der Folienpotentiometer m�ssen auf dem Chipkit Uno Board s�mtliche Pins belegt werden:
|   |   |
|---|---|
|SERVO_PIN | PWM/DIGITAL 3|
|WIPPE_PIN | ANALOG 0|

#### Die Initialisierung
Zu Beginn m�ssen die in der Tabelle belegten Pins initialisiert werden. Damit k�nnen anschlie�end Operationen durchgef�hrt werden. Zus�tzlich werden sp�ter ben�tigte Tasks initialisiert. Diese werden von dem Chipkit Uno32 unterst�tzt.
```c
   pinMode(SERVO_PIN, OUTPUT);
   pinMode(WIPPE_PIN, INPUT);
   pidRegler�ID = createTask(pidRegler, 20, TASK_ENABLE, &task_var);
   printTaskID = createTask(printTask, 1000, TASK_ENABLE, &task_var);
   Serial.begin(9600);
```

#### Implementierung der PWM
Ziel der Implementierung bestand darin einen Servomotor ohne entsprechende Arduino Bibliotheken anzusteuern. Die Idee zur Implementierung der PWM bestand darin Zeitintervalle mit Mikrosekunden festzulegen. Dabei darf der Mikrocontroller nicht mit der zur Verf�gung gestellten "'delay"' Funktion blockiert werden. Im folgenden Codebeispiel wird alle 20 Millisekunden f�r einen bestimmten Intervall die Pinbelegung f�r den Servomotor auf "'Hoch"' gesetzt. 

```c
void servoMove(){
  // Winkel in Mikrosekunden umrechnen
  unsigned long currentMicros = micros();
  d = analogRead(WIPPE_PIN);
  if (currentMicros - previousMicros >= interval){
    //Wenn der Servo kein Signal bekommt, muss ein neuer Interval f�r ein Signal 
    //je nach Regelung ermittelt werden. Andernfalls gibt es kein Signal f�r 
    //20 Millisekunden - Signal Intervall f�r 20ms PWM
    if (state == LOW){
      if(regelung < SERVO_MAX){
        regelung = SERVO_MAX;
      }else if(regelung > SERVO_MIN){
        regelung = SERVO_MIN;
      }
      interval = -regelung+SERVO_MITTE;
      state = HIGH;
    }else{
      state = LOW;
      interval = 20000-interval;
    }
    // Signal oder kein Signal wird an den Servo �bermittelt
    digitalWrite(SERVO_PIN, state);
    previousMicros = currentMicros;    
  }
}
```

#### Die Regelungsfunktion
Die Berechnung der Regelung wird in einem eigenen Task durchgef�hrt. Dabei wird der Wert der Potentiometer eingelesen und die Position berechnet. Anschlie�end m�ssen die entsprechenden Eingangsgr��en f�r die einzelnen Regelungsanteilen ermittelt werden(e, esum, e_alt). Dies geschah durch "probieren"

```c
void pidRegler(int id, void * tptr){
  float messwert = analogRead(WIPPE_PIN);
  strecke = (1-messwert/1023)/(1+messwert/1023);
  float e = WIPPE_SENSOR_MITTE - strecke;
  if( e <= 0.00 && e >= -0.00){
    e = 0;
  }
  
  static float e_alt = 0;
  static float esum = 0;
  
  esum = esum + e;
  
  float u = (Kp * e)+ HochPass((Ki*Ta*esum),0.90) + (Kd * ( e - e_alt)) / Ta;
  e_alt = e;
  
  regelung = u;
}
```

F�r den I-Anteil wurde ein zus�tzlicher Filter verwendet um entsprechend starke Eingangsgr��en etwas abzuschw�chen.

#### Serielle Ausgabe der Position
Im folgenden wurde die Serielle Ausgabe f�r den Abstand der Kugel zur Mitte implementiert. Auch diese Funktion wird als eigene Task ausgef�hrt. Arduino's Serielle Ausgabe besitzt einen Nachteil. Die Funktion "Serial.print()" bremst den Ablauf so stark ab, dass die PWM nicht ordentlich ausgef�hrt wird. Dadurch wird es unm�glich den Servo-Motor richtig zu regulieren.

```c
void printTask(int id, void * tptr){
  pos = (int8_t)((strecke - WIPPE_SENSOR_MITTE) * (DISTANCE / (WIPPE_SENSOR_RECHTS - WIPPE_SENSOR_LINKS)));
  pos = pos < 0 ? pos*(-1) : pos;
  Serial.print(pos,DEC);
  Serial.print(" mm\n");
}
```

### 5. Evaluation

#### Die PWM
Aufgabe war es eine Pulsweitenmodulation zu Implementieren, ohne Den Prozessor zu blockieren. In dieser Arbeit war der Ansatz mittels Mikrosekunden diese zu realisieren. Es gab allerdings bei der Implementierung einige Schwierigkeiten, da die Serielle Ausgabe sehr viel Rechenzeit ben�tigt und w�hrend des Programmablaufs die PWM st�rt. Deshalb wurde die Serielle Ausgabe mittels einem Task implementiert. Mit diesem Task konnte der Ablauf der PWM geschont werden. Ein weiterer Nachteil wird sein, dass bei der Verwendung von Mikrosekunden nach einigen Tagen ein �berlauf stattfinden kann. Dies wurde allerdings in diesem Versuch nicht ber�cksichtigt.

[OsziPWM-Bild]

#### Die Regelung der Wippe
Ziel der Aufgabe war es die Kugel in der Mitte zu Regeln. Entsprechend werden s�mtliche Signale an den Servo-Motor �bermittelt je nach Position der Kugel. Die in Abbildungen 7 und 8 sollen dies zeigen:

[Bild 1]

[Bild 2]

Der PID-Regler der implementiert wurde, Regelt die Kugel deutlich schneller in die Mitte als ein normaler P-Regler. Zu beginn gab es mit dem I-Anteil Schwierigkeiten, da werte Exorbitant hoch berechnet wurden. Dies wurde mit einem Hochpass-Filter entsch�rft.